# Development

The deployments repo defines a skaffold configuration to simplify development.
Using profiles, we can easily deploy configs for each of the three REAPI servers
and rebuild images automatically as the source code changes.

> Note: This currently relies on master branch of skaffold, as it requires
>       multiple kustomize files in the skaffold config.
>
> https://github.com/GoogleContainerTools/skaffold/pull/3585

This repo is the staging ground for adding additional metrics to a number of
build clients. The metrics are:



## Setup

First clone the repo.
You'll then need to compile the `kube-prometheus` configs:

```
git clone --recurse-submodules git@gitlab.com/arlyon-dissertation/development.git
cd development/prometheus
./build.sh
cd ..
```

Sometimes the 4 `CustomResourceDefinition` that `kube-prometheus` defines
don't propagate in time, so you'll need to apply them ahead of time. In that
case just run:

```
find prometheus/manifests/setup -type f -name 'prometheus-operator-*CustomResourceDefinition.yaml' -exec kubectl apply -f {} \;
```
 
## Deployment

To launch a deployment simply run `skaffold dev -p (buildbarn|buildgrid|buildfarm)` 
and then run your job of choice.

## Monitoring

The system is monitored by prometheus and jaeger. Prometheus is primarily concerned
with collecting metrics, whereas jaeger handles tracing across services.

To connect to prometheus, forward the port and navigate to [http://localhost:9090](http://localhost:9090).

```
kubectl port-forward -n monitoring svc/prometheus-k8s 9090
kubectl port-forward -n monitoring svc/grafana 3000
kubectl port-forward -n monitoring svc/alertmanager-main 9093
```

```json
{
    "properties":[
        {"name":"OSFamily","value":"Linux"},
        {"name":"container-image","value":"docker://marketplace.gcr.io/google/rbe-ubuntu16-04@sha256:87e1bb4a47ade8ad4db467a2339bd0081fcf485ec02bcfc3b30309280b38d14b"}
    ]
} 

{{
    "properties":[
        {"name":"OSFamily","value":"Linux"},
        {"name":"container-image","value":"docker://marketplace.gcr.io/google/rbe-ubuntu16-04@sha256:87e1bb4a47ade8ad4db467a2339bd0081fcf485ec02bcfc3b30309280b38d14b"}
    ]
}}
```